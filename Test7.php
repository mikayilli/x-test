<?php

/**
 * 1.
 *
 *  select c.Name, c.city, s.Name Salesman_name from customer c
 *  RIGHT JOIN salesman s ON s.Id = c.Salesman_id
 *  where commission  Between 0.12 and 0.14
 */

/**
 * 2.
 *
 * SELECT * FROM `salesman`
 * WHERE `id` NOT IN (SELECT `customer`.`Salesman_id` FROM `customer`)
 * 
 */
