<?php
/**
 *  NOW() will fetch the current date and time both in format 'YYYY-MM-DD HH:MM:SS'
 *  while CURRENT_DATE() will fetch the date of the current day 'YYYY-MM-DD'.
 */
// SELECT NOW(), CURRENT_DATE()
