/*
 if you call on() this way (1) than you have to be sure that selector already exist on document.
 if it is dynamically created element than need to call on() as example 2.
 */

// 1.
// $( "button" ).on( "click", function() {
//     console.log( "Button Clicked:",this );
// });

// 2.
$( document ).on( "click", "button", function() {
    console.log( "Button Clicked:",this );
});

$( "html" ).append( "<button>Click Alert!</button>" );
