function unique(arr) {
    return Array.from(new Set(arr));
}

console.log(unique([1,2,3,4,1,3,1]));
