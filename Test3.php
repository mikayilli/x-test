<?php
// strpos finds 'yaba' at position 0 , 0 means false . so if need to correct if statement
$str1 = 'yabadabadoo';
$str2 = 'yaba';
if (strpos($str1,$str2) >= 0) {
    echo "\"" . $str1 . "\" contains \"" . $str2 . "\"";
} else {
    echo "\"" . $str1 . "\" does not contain \"" . $str2 . "\"";
}
