<?php

class Test2 {
    public function is_power($number,$base)
    {
        while ($number % $base == 0) {
            $number = $number / $base;
        }

        if($number == 1) return true;

        return false;
    }

    public function format_number($str) {
        preg_match_all('/[0-9,.]+/', $str, $matches);
        return $matches[0][0] ?? 0;
    }

    public function sum_digits($int)
    {
        $result = 0;
        while ($int >= 1)
        {
            $result = $result + $int % 10;
            $int = $int/10;
        }

        return $result;
    }
}

$test2 = new Test2;

//echo $test2->is_power(12,2);

//echo $test2->format_number("$4,000.15A");

//echo $test2->sum_digits(229);

