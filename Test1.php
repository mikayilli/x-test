<?php
class Test1 {
    public function last_word($sentence)
    {
        if(empty($sentence)) return 0;
        return substr($sentence, strrpos($sentence, ' ') + 1);
    }

    public function sql_date_format($date)
    {
        return date("Y-m-d", strtotime($date));
    }

    public function extract_string($str)
    {
        preg_match('/\[(.*?)\]/', $str, $matches);
        return $matches[1] ?? "" ;
    }
}

$test1 = new Test1;

echo $test1->last_word("kain mikayilli") ." \n";

echo $test1->sql_date_format("20-08-2019") ." \n";

print_r($test1->extract_string("hey [world]")) ." \n";
