<?php
/*
 * SELECT a.id, a.name, b.grade
 * FROM a
 * LEFT JOIN b on b.id = a.id
 */

// it should return every record from 'a' table and from 'b' table only 'b.id = a.id' matched records

/**
 * Ali -> 98
 * Moshe -> null
 * Yossi -> 55
 */
