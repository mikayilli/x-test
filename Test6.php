<?php

/*
 * UNION combine 2 or more select queries set to one result and removes duplicates
 * as well as there is UNION ALL , unlike UNION it kees all duplicate
 */

/*
 * select * from a
 * UNION
 * select * from a
 *
 * it should return all record from 'a' table
 */

/*
 * select * from a
 * UNION ALL
 * select * from a
 *
 * it should return all record from 'a' table two time and combine it in one result
 */
